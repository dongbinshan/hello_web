from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from users.models import User
import hashlib
# Create your views here.


def register(request):
    if request.method == 'GET':
        return render(request, 'register.html')
    else:
        import json
        req_dict = json.loads(request.body)
        username = req_dict.get('username')
        password = req_dict.get('password')
        yan = '!@#'
        md5_pwd = hashlib.md5()
        md5_pwd.update((password + yan).encode('UTF-8'))
        password = md5_pwd.hexdigest()

        print('username: %s password: %s' % (username, password))
        user = User.objects.create(username=username, password=password)

        # return redirect('/login/')
        return HttpResponse('注册成功')


def login(request):
    username = request.session.get('username')

    if username:
        # username 在 session 中存在，则用户已登录
        return HttpResponse('%s用户已登录' % username)
    if request.method == 'GET':
        return render(request, 'login.html')

    else:
        # 登录业务逻辑
        # 获取 username 和 password
        username = request.body.get('username')
        password = request.body.get('password')
        remember = request.body.get('remember')
    try:
        user = User.objects.get(username=username, password=password)
    except User.DoesNotExist:
        return JsonResponse({'message': 'login failed'})
    else:
        # 用户名和密码正确
        # Session 中保存当前登录用户的信息
        request.session['user_id'] = user.id
        request.session['username'] = user.username

        # 判断是否记住登录
        if remember != 'true':
            # 不记住登录，将 session的标识cookie 设置为浏览器关闭即失效
            request.session.set_expiry(0)

        return JsonResponse({'message': 'login success'})


def user_info(request, id):
    """获取指定用户的信息"""
    try:
        # 根据 id 获取指定用户的信息
        user = User.objects.get(id=id)
    except User.DoesNotExist:
        # 用户不存在
        return JsonResponse({'message': '用户不存在'})
    else:
        # 通过 json 返回用户的信息数据
        res_data = {
            'id': user.id,
            'name': user.username,
            'gender': user.gender,
            'age': user.age
        }

        return JsonResponse(res_data)

from django.views import View


class LodinView(View):
    def get(self, request):
        username = request.session.get('username')
        if username:
            return HttpResponse('%s 用户已登录' % username)
        return render(request, 'login.html')
    def post(self, request):
        username = request.session.get('username')
        if username:
            return HttpResponse('%s 用户已登录' % username)
        username = request.POST.get('username')
        password = request.POST.get('password')
        remember = request.POST.get('remember')
        try:
            user = User.objects.get(username=username, password=password)
        except User.DoesNotExist:
            return JsonResponse({'message': 'login failed!'})
        else:
            request.session['user_id'] = user.id
            request.session['username'] = user.username
            if remember != 'true':
                request.session.set_expiry(0)
            return JsonResponse({'message':  'login success'})
